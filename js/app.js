$(document).foundation();

$('.slider-home').slick({
	 dots: true,
	 nextArrow : '<button type="button" class="slick-next"><img src="img/arrow-green.png" alt="" /></button>',
	 prevArrow : '<button type="button" class="slick-prev"><img src="img/arrow-green.png" alt="" /></button>'
});

$('.review-slider').slick({
	infinite: true,
	speed: 300,
	slidesToShow: 1,
	centerMode: true,
	nextArrow : '<button type="button" class="slick-next"><img src="img/arrow-white.png" alt="" /></button>',
	prevArrow : '<button type="button" class="slick-prev"><img src="img/arrow-white.png" alt="" /></button>',
	responsive: [
		{
		  breakpoint: 480,
		  settings: {
		    centerMode: false
		  }
		}
	]
});

$('.circle').click(function(){
	$('.circle-active').removeClass('circle-active');
	$(this).parent().addClass('circle-active');
});
$('.mobile-nav').click(function(e){
	e.preventDefault();
	$('.menu-wrap').stop().slideToggle();
})